/**
 *<p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : </li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月02日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
import ContentWrapper from './ContentWrapper';
import Button from './Button';
import Avatar from './Avatar';
import PrettyCheckbox from 'pretty-checkbox-vue';
import vSelect from 'vue-select';
import Snotify, {SnotifyPosition} from 'vue-snotify';
import EasyUI from 'vx-easyui';

export default (Vue) => {
  Vue.component('content-wrapper', ContentWrapper);
  Vue.component('bs-button', Button);
  Vue.component('v-select', vSelect);
  Vue.component('bs-avatar', Avatar);
  Vue.use(PrettyCheckbox);
  Vue.use(EasyUI);
  Vue.use(Snotify, {
    toast: {
      position: SnotifyPosition.rightTop,
      showProgressBar: false
    }
  });
};
